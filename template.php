<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
function gnwt_base_theme_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  // gnwt_bearnet_preprocess_html($variables, $hook);
  gnwt_base_theme_preprocess_page($variables, $hook);
}

/**
 * Override or insert variables into the html templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  $variables['classes_array'] = array_diff($variables['classes_array'],
    array('class-to-remove')
  );
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function gnwt_base_theme_preprocess_page(&$variables, $hook) {
  // Create shorthand variable for path to the theme's img directory
  $variables['img_path'] = base_path() . drupal_get_path('theme', 'gnwt_base_theme') . '/images/';

  // Set a default language in case it is not set below
  $variables['locale'] = 'en';
  if (array_key_exists('language', $variables)) {
    // If the language has been set for the page, set the locale to be the two
    // letter code to be used in the page template.
    $variables['locale'] = $variables['language']->language;
  }

  // Shorthand.
  $first = !empty($variables['page']['sidebar_first']);
  $second = !empty($variables['page']['sidebar_second']);
  // Set defaults for variables.
  $variables['content_class'] = 'layout-3col__full';
  $variables['sidebar_first_class'] = $variables['sidebar_second_class'] = '';
  // Define variables based on when first and second sidebars are displayed.
  if ($first && $second) {
    $variables['content_class'] = 'layout-3col__right-content';
    $variables['sidebar_first_class'] = 'layout-3col__first-left-sidebar';
    $variables['sidebar_second_class'] = 'layout-3col__second-left-sidebar';
  } elseif ($second) {
    $variables['content_class'] = 'layout-3col__left-content';
    $variables['sidebar_second_class'] = 'layout-3col__right-sidebar';
  } elseif ($first) {
    $variables['content_class'] = 'layout-3col__right-content';
    $variables['sidebar_first_class'] = 'layout-3col__left-sidebar';
  }

  $variables['display_layout'] = _gnwt_base_theme_display_layout($variables);

  // When the Full Width Banner exists on the homepage, hide the banner and
  // minister regions.
  if (!empty($variables['page']['banner_full'])) {
    $variables['page']['banner'] = $variables['page']['minister'] = array();
  }

  // Manage background images in theme settings.
  $backgrounds = array(
    'highlight_bg' => array(
      'default_path' => $variables['img_path'] . "curve.png",
      'target_classes' => array('highlighted'),
    ),
    // Using `.region-banner-full > .block` as an additional target class for
    // this global implementation, since the default `.banner` class will not
    // be embedded in the HTML at this time.
    'banner_bg' => array(
      'default_path' => $variables['img_path'] . "banner-bg.png",
      'target_classes' => array('banner', 'region-banner-full > .block'),
    ),
  );

  foreach ($backgrounds as $bg => $settings) {
    $bg_image_path = FALSE;
    if (theme_get_setting('gnwt_default_' . $bg)) {
      // Display the default highlight background image when this is set.
      $bg_image_path = $settings['default_path'];
    }
    else if ($bg_fid = theme_get_setting('gnwt_' . $bg)) {
      $image = file_load($bg_fid);
      if (is_object($image)) {
        if ($url = file_create_url($image->uri)) {
          $bg_image_path = $url;
        }
      }
    }

    // Begin setting CSS properties.
    $properties = array();
    if ($bg_image_path) {
      $properties[] = "background-image: url('$bg_image_path');";
    }
    if ($bg_color = theme_get_setting('gnwt_' . $bg . '_color')) {
      $properties[] = "background-color: " . check_plain($bg_color) . ";";
    }
    // If properties exist on the target classes, add them as inline css.
    if (!empty($properties)) {
      $classes = array_map(function($val) { return "." . $val; }, $settings['target_classes']);

      // By applying a group and weight we can ensure this loads last in the CSS
      // chain.
      drupal_add_css(implode(",", $classes) . "{" . implode(" ", $properties) . "}", array(
        'type' => 'inline',
        'group' => CSS_THEME,
        'weight' => 999,
      ));
    }
  }
}

/**
 * Check if the layout section of the page should be displayed.
 *
 * @param array $variables
 *   The $variables array passed into the page preprocessor.
 *
 * @return boolean
 */
function _gnwt_base_theme_display_layout($variables) {
  // Check if `gnwt_front_page_content_region` is set to display.
  if (!$variables['is_front'] || theme_get_setting('gnwt_front_page_content_region')) {
    return TRUE;
  }

  // Array keys from $page var to check for existence in output. The 'content'
  // key doesn't need to be checked since it will always output stock Drupal
  // teasers, and we check for that with the 'gnwt_front_page_content_region'
  // variable instead.
  $page_keys = array(
    'content_top',
    'help',
    'content_below_title',
    'content_bottom',
    'sidebar_first',
    'sidebar_second',
  );
  // Array keys from $variables to check for existence in output.
  $render_keys = array(
    'tabs',
    'action_links',
  );

  foreach ($page_keys as $key) {
    // These render arrays will be empty if there is no content to output.
    if (!empty($variables['page'][$key])) {
      return TRUE;
    }
  }

  foreach ($render_keys as $key => $is_render) {
    // These render arrays may be populated even if there is no content to
    // output. As a result, we check for the render first.
    if ($render = render($variables[$key])) {
      return TRUE;
    }
  }

  return FALSE;
}



/**
 * Override or insert variables into the region templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--no-wrapper.tpl.php template for sidebars.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('region__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('block__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * @file
 * Theme functions for the Facet API module.
 */

/**
 * Returns HTML for an inactive facet item.
 *
 * @param $variables
 *   An associative array containing the keys 'text', 'path', 'options', and
 *   'count'. See the l() and gnwt_base_theme_facetapi_count() functions for information
 *   about these variables.
 *
 * @ingroup themeable
 */
function gnwt_base_theme_facetapi_link_inactive($variables) {
  // Builds accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => FALSE,
  );
  $accessible_markup = theme('facetapi_accessible_markup', $accessible_vars);

  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $variables['text'] = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Adds count to link if one was passed.
  if (isset($variables['count'])) {
    $variables['text'] .= ' ' . theme('facetapi_count', $variables);
  }

  // Resets link text, sets to options to HTML since we already sanitized the
  // link text and are providing additional markup for accessibility.
  $variables['text'] .= $accessible_markup;
  $variables['options']['html'] = TRUE;
  $variables['options']['attributes']['class'][] = 'facetapi-link';
  return theme_link($variables);
}

/**
 * Returns HTML for the active facet item's count.
 *
 * @param $variables
 *   An associative array containing:
 *   - count: The item's facet count.
 *
 * @ingroup themeable
 */
function gnwt_base_theme_facetapi_count($variables) {
  return '<span class="facet-count">' . (int) $variables['count'] . '</span>';
}

/**
 * Returns HTML for an active facet item.
 *
 * @param $variables
 *   An associative array containing the keys 'text', 'path', and 'options'. See
 *   the l() function for information about these variables.
 *
 * @see l()
 *
 * @ingroup themeable
 */
function gnwt_base_theme_facetapi_link_active($variables) {

  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => theme('facetapi_deactivate_widget', $variables),
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements) . $link_text;
  $variables['options']['html'] = TRUE;
  $variables['options']['attributes']['class'][] = 'facetapi-link';
  return theme_link($variables);
}
/**
 * Returns HTML for the deactivation widget.
 *
 * @param $variables
 *   An associative array containing the keys 'text', 'path', and 'options'. See
 *   the l() function for information about these variables.
 *
 * @see l()
 * @see gnwt_sd2_theme_facetapi_link_active()
 *
 * @ingroup themable
 */
function gnwt_base_theme_facetapi_deactivate_widget($variables) {
  return '<span class="element-invisible">X</span>';
}

/**
 * Theme the feed link for Views Data Export.
 *
 * Override the output of the feed icon to make it a button with text from the
 * view settings instead.
 */
function gnwt_base_theme_views_data_export_feed_icon($variables) {
  extract($variables, EXTR_SKIP);
  $url_options = array(
    'html' => FALSE,
    'attributes' => array(
      'class' => array('button'),
    ),
  );
  if ($query) {
    $url_options['query'] = $query;
  }
  return l($text, $url, $url_options);
}


/**
 * Implements hook_views_pre_render().
 *
 * For some reason, the views_slideshow does not hide the controls when only 1
 * slide exists, despite the fact that the feature exists. Here, we try to
 * manually override that so that if there's only 1 slide, let's hide the
 * controls. This requires that the 'hide on single slide' setting be enabled.
 */
function gnwt_base_theme_views_pre_render(&$view) {
  // Homepage Slider.
  if ($view->name == 'home_slider' && $view->current_display == 'block') {
    if ($view->style_plugin->options['widgets']['bottom']['views_slideshow_controls']['hide_on_single_slide'] && count($view->result) < 2) {
      $view->style_plugin->options['widgets']['bottom']['views_slideshow_controls']['enable'] = 0;
    }
  }
}
