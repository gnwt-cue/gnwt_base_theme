<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */

// Render metatags for front page when content is not rendered.
// @see https://www.drupal.org/project/metatag/issues/1386320
if ($is_front && !theme_get_setting('gnwt_front_page_content_region')) {
  render($page['content']['metatags']);
}
?>

<div id="page">
  <div id="inner-page" >
    <div id="top-wrapper">
      <div id="top-nav" class="layout-center" >
        <?php print render($page['top']); ?>
        <?php print render($page['top_language']); ?>
        <?php print render($page['top_search']); ?>
      </div>
    </div>
    <header class="header" id="header" role="banner">
      <div class="container layout-center">

        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo" id="logo">
            <img src="<?php print $logo; ?>" srcset="<?php print $logo; ?> 1x, <?php print $img_path; ?>logo@2x.png 2x, <?php print $img_path; ?>logo@3x.png 3x" alt="<?php print t('Logo of the Government of Northwest Territories'); ?>" class="header__logo-image" />
          </a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <div class="header__name-and-slogan" id="name-and-slogan">
            <?php if ($site_name): ?>
            <h1 class="header__site-name" id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <?php print render($page['header']); ?>

      </div>

    </header>

    <?php print render($page['header_bottom']); ?>

    <div id="mobile-wrap">
      <a href="#" class="mobile-menu-button"><?php print t('Menu'); ?>
        <span class="menu glyphicon glyphicon-list"></span>
        <?php print t('Search'); ?><span class="search glyphicon glyphicon-search"></span></a>
      <div id="mobile-menu" class="rm-hidden">
        <h3 class="title"></h3>
        <div class="inner">
          <?php print render($page['mobile_search']); ?>
          <?php print render($page['mobile_language']); ?>
          <?php print render($page['mobile_sub']); ?>
          <?php print render($page['mobile_main']); ?>
          <?php print render($page['mobile_bottom']); ?>
        </div>
      </div>
    </div>

    <div id="navigation">
      <nav id="main-menu" role="navigation" tabindex="-1" >
        <div class="layout-center">
          <?php print render($page['navigation']); ?>
        </div>
      </nav>
    </div>

    <div id="breadcrumbs">
      <div class="breadcrumb layout-center">
        <?php print render($page['breadcrumbs']); ?>
        <?php print $breadcrumb; ?>
      </div>
    </div>

    <?php if (!empty($page['banner_full'])): ?>
      <?php print render($page['banner_full']); ?>
    <?php endif; ?>

    <?php if ($is_front): ?>
      <?php
        // Render the sidebars to see if there's anything in them.
        $banner  = render($page['banner']);
        $minister = render($page['minister']);
        $highlighted  = render($page['highlighted']);
      ?>
      <?php if($banner || $minister): ?>
        <div id="banner-minister" class="layout-3col layout-center">
          <div class="layout-3col__left-content">
            <?php print $banner; ?>
          </div>
          <div class="layout-3col__right-sidebar">
            <?php print $minister; ?>
          </div>
        </div>
      <?php endif; ?>

      <?php if ($highlighted): ?>
        <div id="highlighted" class="highlighted highlighted-top">
          <div class="layout-center">
            <?php print $highlighted; ?>
          </div>
        </div>
      <?php endif; ?>

    <?php endif; ?>

    <?php if ($display_layout || !empty($messages)): ?>
    <div class="layout-center layout-3col layout-swap">
      <main id="main" role="main">
        <div id="content" class="<?php print $content_class; ?> main-content column" role="main">
          <a id="main-content"></a>
          <?php print render($page['content_top']); ?>
          <?php print render($title_prefix); ?>
          <?php if ($title && (!$is_front || theme_get_setting('gnwt_front_page_content_region'))): ?>
            <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          <?php print render($tabs); ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          <?php print render($page['content_below_title']); ?>
          <?php if (!$is_front || theme_get_setting('gnwt_front_page_content_region')): ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
          <?php endif; ?>
          <?php print render($page['content_bottom']); ?>
        </div>
      </main>

      <?php if (!empty($page['sidebar_first'])): ?>
        <aside class="<?php print $sidebar_first_class; ?>" role="complementary">
          <?php print render($page['sidebar_first']); ?>
        </aside>
      <?php endif; ?>

      <?php if (!empty($page['sidebar_second'])): ?>
        <aside class="<?php print $sidebar_second_class; ?>" role="complementary">
          <?php print render($page['sidebar_second']); ?>
        </aside>
      <?php endif; ?>
    </div>
    <?php endif; ?>

  </div>
</div>

<footer id="footer">
  <div class="footer-container layout-center">
    <?php print render($page['footer']); ?>
  </div>

  <div class="layout-center footer-wordmark" style="text-align:right;">
    <?php $wordmark = $img_path . 'gnwt-wordmark-' . $locale; ?>
    <img src="<?php print $wordmark; ?>.png" srcset="<?php print $wordmark; ?>.png 1x, <?php print $wordmark; ?>@2x.png 2x, <?php print $wordmark; ?>@3x.png 3x" alt="<?php print t('Government of Northwest Territories'); ?>" class="logo">
  </div>
</footer>

<?php print render($page['bottom']); ?>
