<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="html <?php print $classes; ?>">

  <div id="page">
    <div id="inner-page">
      <div id="top-wrapper">
        <div id="top-nav" class="layout-center">
          <div class="region region-top">
            <section id="block-menu-menu-top-gnwt-menu" class="block block-menu block-delta-menu-top-gnwt-menu clearfix">
              <ul class="menu nav">
                <li class="first leaf"><a href="http://www.gov.nt.ca/" title="">www.gov.nt.ca</a></li>
                <li class="leaf"><a href="http://www.gov.nt.ca/research/departments/" title="">Departments</a></li>
                <li class="leaf"><a href="http://services.exec.gov.nt.ca/" title="">Services</a></li>
                <li class="last leaf"><a href="http://www.gov.nt.ca/utility/contact/index.html" title="">Contact</a></li>
              </ul>
            </section> <!-- /.block -->
          </div>
        </div>
      </div>

      <div class="maintenance-header">
        <header id="header" class="header" role="banner">
          <div class="container layout-center">
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo" id="logo">
              <img src="<?php print $logo; ?>" srcset="<?php print $logo; ?> 1x, <?php print $img_path; ?>logo@2x.png 2x, <?php print $img_path; ?>logo@3x.png 3x" alt="<?php print t('Logo of the Government of Northwest Territories'); ?>" class="header__logo-image" />
            </a>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <div class="header__name-and-slogan" id="name-and-slogan">
              <?php if ($site_name): ?>
                <h1 class="header__site-name" id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          </div>
        </header>
      </div>

      <div class="layout-center layout-3col layout-swap">
        <div id="main">

          <?php if (!empty($title)): ?>
            <h1 class="page-header"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <div class="maintenance-msg">
            <?php print $content; ?>
          </div>

        </div>
      </div>

    </div>

  </div>

  <footer id="footer" class="maintenance-footer">
    <div class="footer-container layout-center">
      <?php if (!empty($footer)): print $footer; endif; ?>
    </div>

    <div class="layout-center footer-wordmark" style="text-align:right;">
      <?php $wordmark = $img_path . 'gnwt-wordmark-' . $locale; ?>
      <img src="<?php print $wordmark; ?>.png" srcset="<?php print $wordmark; ?>.png 1x, <?php print $wordmark; ?>@2x.png 2x, <?php print $wordmark; ?>@3x.png 3x" alt="<?php print t('Government of Northwest Territories'); ?>" class="logo">
    </div>
  </footer>

</body>
</html>
