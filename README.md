# Creating Theme From GNWT Base Theme
### Step 1: 
Pull gnwt\_base\_theme from bitbucket repo here: ```git clone git@bitbucket.org:gnwt-cue/gnwt_base_theme.git```

### Step 2: 
Ensure that the gnwt\_base\_theme appears in the sites/all/themes directory for single site installations. For multisites, the gnwt\_base\_theme may appear in sites/all/themes, or in the sites/new-site/themes directory at your discretion.

### Step 3:
Run command ```drush gnwttheme new_theme_name --path=sites/all/themes```

As stated before, the path may vary based on whether this is a single or multisite installation. In general for multisites these new themes will be tailored to individual sites so it usually makes the most sense to install the new theme in the sites/newsite/themes directory instead of sites/all/themes. After a directory is chosen, open ```gulpfile.js``` and edit the line ```parent: '../gnwt_base_theme/' ``` to wherever the gnwt\_base\_theme is located.

### Step 4:
Assuming node and npm are both installed in your working environment, run the command ```npm install``` from the root of the new theme.

This will get all of the packages from the package.json file and install them as node modules.

### Step 5:
Enable the new theme using either drush or the durpal admin site interface. The cache may need to be cleared to see changes.

### The theme is now created and ready to be styled.
The zen theme contains documentation on how to override sub themes in general. See https://www.drupal.org/documentation/theme/zen as well as the zen 7.6 readme for more information.

Styling the new theme will involve overriding the sass directory, as well as creating new sass files not in the gnwt_base_directory. To override a file that already exists, copy the file into the new theme using the same directory and file names as the gnwt_base_theme. Then make the changes to the file in the new theme. Finally run ```gulp styles``` to recompile the css. ```gulp watch``` will not work with the current gnwt base theme.

If a new file needs to be added, it should be created in the proper directory, and named with a preceding underscore (ie ```_newfile.scss```). The new file will need to be imported in the _gnwt-core.scss file to be added to the main css. This is done by adding the line ```@import 'newfile'``` to the _gnwt-core.scss file. Again the ```gulp styles``` command should be run to recompile the css. 
