/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {
  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.gnwt_base_theme = {
    attach: function (context, settings) {
      var gnwt = this;

      // Include context so it only runs when conditions are met.
      $(document, context).ready(function () {
        setTimeout(function () {
          gnwt.displaceCurve();
        }, 300);

        $('body').on('onresize', function () {
          gnwt.displaceCurve();
        });

        // @TODO This should be removed in favour of inserting into page.tpl.php.
        $('#mobile-menu .title').append('<div class="close glyphicon glyphicon-remove-circle"></div>');

        if (jQuery().superfish) {
          $('.sf-js-enabled').superfish({
            delay: 800,
            animation: {opacity: 'show', height: 'show'},
            speed: 'fast',
            autoArrows: false,
            dropShadows: false,
            disableHI: true
          });
        }

      });
    },

    displaceCurve: function () {
      // Curve should be 190 pixels up from the bottom of the footer. Including
      // margin from body covers additional menus.
      var bgTop = $('footer#footer').position().top + $('footer#footer').height() - 190 + parseInt($('body').css('margin-top'));
      $('body').css({'background-position': 'center ' + bgTop + 'px'});
    }
  };

  Drupal.behaviors.gnwtMobile = {
    attach: function (context, settings) {

      if (!$('html').hasClass('lt-ie9')) {
        $('.mobile-menu-button, #mobile-menu .title').click(function () {
          $('#mobile-menu').toggle();
          $('.mobile-menu-button').toggleClass('open');
        });
        $('#mobile-wrap li > a').on('touchstart', function (e) {
          $(this).addClass('hover');
        }).on('touchmove', function (e) {
          $(this).removeClass('hover');
        }).mouseenter(function (e) {
          $(this).addClass('hover');
        }).mouseleave(function (e) {
          $(this).removeClass('hover');
        }).click(function (e) {
          $(this).removeClass('hover');
        });
      }
    }
  };

  Drupal.behaviors.gnwtLogin = {
    attach: function (context, settings) {
      $('#user-login input[name=name]', context).focus();
    }
  };

})(jQuery, Drupal, this, this.document);
