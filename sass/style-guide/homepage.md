# GNWT Base Theme Style Guide

**Current Version:** 7.x-6.4-components

This style guide documents the designs of this website which are built with component-based styles and Sass variables, functions and mixins. To ensure it is always up-to-date, this style guide is automatically generated from comments in the Sass files.

## Organization

Design components are reusable designs that can be applied using just the CSS class names specified in the component. We categorize our components to make them easy to find.

<dl>
<dt><strong>Defaults</strong></dt>
<dd><code>sass/base</code> — The default “base” components apply to HTML elements. Since all of the rulesets in this class of styles are HTML elements, the styles apply automatically.</dd>
<dt><strong>Layouts</strong></dt>
<dd><code>sass/layouts</code> — Layout components position major chunks of the page. They just apply positioning, no other styles.</dd>
<dt><strong>Components</strong></dt>
<dd><code>sass/components</code> — Miscellaneous components are grouped together, but feel free to further categorize these.</dd>
<dt><strong>Navigation</strong></dt>
<dd><code>sass/navigation</code> — Navigation components are specialized design components that are applied to website navigation.</dd>
<dt><strong>Forms</strong></dt>
<dd><code>sass/forms</code> — Form components are specialized design components that are applied to forms or form elements.</dd>
</dl>

In addition to the components, our component library also contains these folders:

<dl>
<dt><strong>Colors and Sass</strong></dt>
<dd><code>sass/init</code> — This Sass documents the colors used throughout the site and various Sass variables, functions and mixins. It also initializes everything we need for all other Sass files: variables, 3rd-party libraries, custom mixins and custom functions.</dd>
<dt><strong>Style guide helper files</strong></dt>
<dd><code>sass/style-guide</code> — files needed to build this automated style guide; includes some CSS overrides for the default KSS style guide</dd>
</dl>
