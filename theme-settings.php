<?php
/**
 * @file
 * Contains the theme's settings form.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * Use managed files to handle uploaded images in the settings form since it
 * fixes a bug with the system settings in Drupal Core.
 *
 * @see http://ghosty.co.uk/2014/03/managed-file-upload-in-drupal-theme-settings/
 */
function gnwt_base_theme_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  // Provide an option for sites to display the content region on the front
  // page. This is useful when microsites want to simply display a node as
  // the homepage content.
  $form['front'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page'),
  );
  $form['front']['gnwt_front_page_content_region'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the <em>Content</em> region on the front page'),
    '#default_value' => theme_get_setting('gnwt_front_page_content_region'),
    '#description' => t('Enable this setting if this site requires that the <em>Content</em> region be displayed on the homepage. This is off by default.'),
  );

  // Setup the Highlight Background Image in the settings form.
  $form['backgrounds'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background Images'),
  );
  $form['backgrounds']['gnwt_default_highlight_bg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default Highlight region background'),
    '#default_value' => theme_get_setting('gnwt_default_highlight_bg'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the default GNWT Curve-in-Motion background image in the Highlight region.'),
  );
  $form['backgrounds']['highlight_bg_settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the highlight background settings when using the default bg.
      'invisible' => array(
        'input[name="gnwt_default_highlight_bg"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['backgrounds']['highlight_bg_settings']['gnwt_highlight_bg'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload highlight background image'),
    '#description' => t('Use this field to upload your highlight background image.'),
    '#upload_location' => 'public://gnwt-base-theme/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
    '#default_value' => theme_get_setting('gnwt_highlight_bg'),
  );
  // Background color may be required when setting a custom background image.
  $form['backgrounds']['highlight_bg_settings']['gnwt_highlight_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Highlight background color (Hex value)'),
    '#description' => t('If required, set the background color value for the highlighted region.'),
    '#default_value' => theme_get_setting('gnwt_highlight_bg_color'),
  );

  // Setup the Banner headling background image in the settings form.
  $form['backgrounds']['gnwt_default_banner_bg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default Banner region background'),
    '#default_value' => theme_get_setting('gnwt_default_banner_bg'),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the default GNWT background image in the Banner region.'),
  );
  $form['backgrounds']['banner_bg_settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the highlight background settings when using the default bg.
      'invisible' => array(
        'input[name="gnwt_default_banner_bg"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['backgrounds']['banner_bg_settings']['gnwt_banner_bg'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload banner background image'),
    '#description' => t("Use this field to upload your banner background image."),
    '#upload_location' => 'public://gnwt-base-theme/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
    '#default_value' => theme_get_setting('gnwt_banner_bg'),
  );

  // Add submission handler to the form.
  $form['#submit'][] = 'gnwt_base_theme_settings_submit';

  // Specify theme settings file to avoid the bug in system theme settings.
  $themes = list_themes();
  // Get the current theme
  $active_theme = $GLOBALS['theme_key'];
  $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';
}

/**
 * Submit Handler: GNWT Base Theme settings form.
 *
 * Make any image files that were uploaded permanent in the system.
 */
function gnwt_base_theme_settings_submit($form, &$form_state) {
  foreach (array('gnwt_highlight_bg', 'gnwt_banner_bg') as $image_field) {
    if (!empty($form_state['values'][$image_field])) {
      $image = file_load($form_state['values'][$image_field]);
      if (is_object($image)) {
        // Check to make sure that the file is set to be permanent.
        if ($image->status == 0) {
          // Update the status.
          $image->status = FILE_STATUS_PERMANENT;
          // Save the update.
          file_save($image);
          // Add a reference to prevent warnings.
          file_usage_add($image, 'gnwt_base_theme', 'theme', 1);
        }
      }
    }
  }
}
